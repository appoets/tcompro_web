<?php

return array(

    'IOSUser'     => array(
        'environment' =>env('IOS_USER_ENV', 'development'),
        'certificate' => app_path().'/apns/user/Instauser.pem',
        'passPhrase'  => env('IOS_PUSH_PASS', 'Appoets123$'),
        'service'     =>'apns'
    ),
    'IOSProvider' => array(
        'environment' => env('IOS_PROVIDER_ENV', 'development'),
        'certificate' => app_path().'/apns/provider/instacartDelivery.pem',
        'passPhrase'  => env('IOS_PROVIDER_PUSH_PASS', 'Appoets123$'),
        'service'     => 'apns'
    ),
    'IOSShop' => array(
        'environment' => env('IOS_SHOP_ENV', 'development'),
        'certificate' => app_path().'/apns/shop/Certificates.pem',
        'passPhrase'  => env('IOS_SHOP_PUSH_PASS', 'Appoets123$'),
        'service'     => 'apns'
    ),
    'AndroidUser' => array(
        'environment' =>env('ANDROID_ENV', 'development'),
        'apiKey'      =>'AAAAkIN5BLI:APA91bFVlTVHoNIUpnwPK3GN26i3oIJ24HjK8cmHkdjKhmuTBwM3Dupd0ZKHLq8cTkmbVSveLmggj1Ww3aG8Ot0e3DdDys4GTNXTWPuSZbBGI3uToLRoe2ZMxe2E_hgT0p37PhuIIpHw',
        'service'     =>'gcm'
    )

);
