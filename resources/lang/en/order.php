<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shop Language Lines
    |--------------------------------------------------------------------------
    */
    
        'order_created' => 'Su Orden A Sido Creada Satisfactoriamente',
        'order_accept_shop' => 'Su Orden Ha Sido Aceptada Por La Tienda',
        'order_cancel_shop' => 'La Tienda Ha Reportado un Error en Su Orden',
        'order_assigned_deliveryboy_user' => 'Su Orden A Sido Asignada A Un Tcomprador',
        'order_assigned_deliveryboy' => 'Nueva Solicitud De Pedido',
        'order_accept_deliveryboy' => 'Su Orden Esta Lista Para Ser Despechada',
        'order_cancel_deliveryboy' => 'Esperando Por El Tcomprador ',
        'order_deliveryboy_reached' => 'Tu Tcomprador Ha sido asignado',
        'order_pickedup' => 'Tu orden ha Sido Tomada',
        'order_arrived' => 'Tu Mercado Ha llegado a tu Casa',
        'order_completed' => 'Tu Orden ha Sido Marcada Como Entregada',
        'order_user_cancel' => 'Tu Orden Ha sido Cancelada',
        'order_dispute' => 'Tu disputa Ha sido Creada',
        'order_status' => 'El estado de tu orden con Id es :status',
        'created' => 'Tu Pedido ha sido Realizado Correctamente',
        'order_can_not_update' => 'No ha sido posible Actualizar tu orden, por favor intentelo Mas tarde',
        'not_paid' => 'Tu Pago No Ha sido completado',
        'address_out_of_range' => 'La direccion esta fuera de nuestra area de cobertura',
        'dispute_created' => 'Tu Queja Ha sido Creada Para tu Orden ',
        'dispute_resolved' => 'Dispute Resolved Successfully',
        'order_settled_by_admin' => 'Se Establecio tu Pago Correctamente',
        'invalid_order' => 'La Orden Seleccionada Es Invalida',
        'order_otp' => 'Tu orden entregada es Otp es :otp',
        'order_otp_mismatch' => 'Por Favor Verifique su Otp',
        'incoming_request' => 'Usted tiene una orden Pendiente',
        'reorder_created' => 'Para Reordenar su orden Por favor proceder.',
        'order_ready_transporter_shop' => 'La Orden Esta Lista Para Ser Entregada',
        'order_ready_user_shop' => 'Tu orden esta lista para ser entregada',
        'review' => [
            'create_review' => 'Comentario Creado Correctamente',
        ],
        'no_delivary_boy' => 'En El Momento No tenemos Tcompradores Disponibles!',
        'invalid' => 'Orden Invalida!',
        'order_shop_not_found' => 'Su Mercado No Esta Listo Todavia Por Favor Intentar Mas Tarde',
        'dispatcher' => [   
            'pending_title' => 'ORDENES PENDIENTES',
            'accept_title' => 'ORDENES ACEPTADAS',
            'ongoing_title' => 'ORDENES EN PROCESO',
            'cancel_title' => 'ORDENES CANCELADAS',
            'order_pending' => 'Lista de ordenes pendientes de clientes',
            'order_accept' => 'ORDENES ACEPTADAS',
            'order_ongoing' => 'ORDENES EN PROCESO',
            'order_cancel' => 'ORDENES CANCELADAS',
            'order_not_found' => 'NO TIENE SERVICIOS SERVICIOS PENDIENTES EN EL MOMENTO!',
            'assign' => 'ASIGNAR',
            'assigned' => 'AsIGNADA',
            'incoming_request' => 'ORDENES EN PROCESO',
            'dispute' => 'PQRS',
            'packed' => 'ASIGNADO',
            'reached' => 'TOMADO',
            'arrived' => 'HA LLEGADO',
            'waiting' => 'ESPERANDO',
            'processing' => 'SE DIRIJE HACIA SU DESTINO',
            'order_live_tracking' => 'Seguimiento en vivo de la ruta'

        ],
        'card' => [
            'no_card_exist' => 'Tarjeta de Credio no existe!'
        ],
        'payment' => [

            'added_to_your_wallet' => 'Agregada a tu billetera',
            'failed' => 'Pago fallido',

        ]

    
      

];
