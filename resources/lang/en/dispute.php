<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lineas de lenguaje de las tiendas 
    |--------------------------------------------------------------------------
    */
    
        'index' => [
            'title' => 'Mensaje comun para presentar un PQR',
            'add_message' => 'agregar mensaje o PQR',
            'sl_no' => 'Sl.No',
            'message' => 'mensajes',
            'action' => 'Action',
            'no_record_found' => 'no tiene mensajes ni PQR a la fecha'
        ],
        'create' => [
            'title' => 'Agregar PQR o solicitar ayuda',
            'message' => 'Mensaje',
            'dispute_type' => 'Tipo de Queja o PQR',
            'cancel' => 'Cancelar',
            'save' => 'Salvar o guardar',
        ],
        'edit' => [
            'title' => 'Editar tienda',
        ],
        'created_success' => 'mensaje o PQR creado satisfactoriamente',
        'updated_success' => 'mensaje o PQR actualizado satisfactoriamente',
        'removed_success' => 'mensaje o PQR borrado satisfactoriamente',
        'user' => [
            'created_success' => 'usuario para mensajes o PQR creado satisfactoriamente',
            'updated_success' => 'usuario para mensajes a sido actualizado correctamente',
            'removed_success' => 'usuario ha sido borrado correctamente',

        ],
        'admin' => [
            'updated_success' => 'El administrador ha sido actualizado correctamente',
        ]
        
      

];
