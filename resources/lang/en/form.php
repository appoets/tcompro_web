<?php

return [

    /*
    |--------------------------------------------------------------------------
    | todos los elementos de los formularios
    |--------------------------------------------------------------------------
    */

    'name' => 'Nombre',
    'first_name' => 'Primer Nombre',
    'last_name' => 'Apellido',
    'email' => 'Direccion de correo electronico',
    'parent' => 'Padres',
    'phone' => 'Numero de celular',
    'password' => 'contraseña',
    'confirm_password' => 'confirmar contraseña',
    'location' => 'ciudad',
    'address' => 'Direccion',
    'logout_success'=>'su sesion ha sido cerrada',
    'hours_opening' => 'horarios de apertura de la tienda',
    'hours_closing' => 'horarios de cierre de la tienda',
    'Min_Amount' => 'compra minima',
    'offer_percent' => 'porcentaje de descuento',
    'estimated_delivery_time' => 'Maximo tiempo de entrega del pedido',
    'cuisine' => 'Cocina',
    'everyday' => 'todos los dias',
    'status' => 'estado de la tienda',
    'description' => 'descripcion',
    'pure_veg' => 'es 100% natural ?',
    'no_data_found' => 'no se cuentran los registros !',
    'usuario_ya_existe' => '

',
    'usuario_no_existe' => 'usuario no registrado. por favor registrarse',
    'Numero_existente' => 'Numero telefonico ya se encuentra registrado',
    'already_favorite' => 'Marcar esta tiena como una de mis favoritas',
    'rating' => [
        'rating_success' => 'Gracias por calificarnos'
    ],
    'resource' => [
        'created' => 'creado correctamente',
        'updated' => 'actualizado correctamente',
        'deleted' => 'borrado correctamente',    
    ],
    'profile' => [
        'updated'=>'su contraseña ha sido actualizada correctamente'
    ],
    'shift' => [
        'shift_end_error'=> 'Todavia no puede cerrar su orden por favor valide los datos o continue realizando sus compras',
        'shift_start_error' => 'debe completar el pedido antes de tomar un descanso'
    ],
    'order' => [
        'cart_empty' => 'Su carrito de compras se encuentra vacio!',
        'not_paid' => 'su pago no se ha completado todavia',
        'insufficient_balance' => 'no cuenta con fondos suficientes'
    ],
    'promocode' => [
        'expired' => 'El codigo promocional se encuentra vencido',
        'applied' => 'codigo promocional ha sido aplicado',
        'already_in_use' => 'El codigo promocional ya ha sido usado',
        'message' => 'Su dinero ha sido acreditado usando un codigo de promocion'
    ],
     'invoice' => [
        'message' => ':El precio de su orden sera debitado en la orden con numero:numero de orden',
    ],
    'dispute' => [

        'messages' => [
            'status' => 'orden :el numero de la orden ha cambiado de CREADO A RESUELTO',
            'price'  => ':El valor de su orden ha sido acreditado con el numero de orden :numero de orden',
            'order_status' => 'Order :order_id  Status Updated To :status'
        ],
        'price' =>' :El precio a sido agregado a su billetera' ,
        'created' => 'Disputa creada con exito',
        'updated' => 'El satatus de su disputa ha sido actualizada con exito',
    ],

    'not_found' => 'No se encontro el recurso solicitado!',
    'whoops' => 'Whoops! Algo no anda bien por favor intentalo de nuevo!',

    'favorite' => [
            'favorite' => 'Ha sido agregado a favoritos correctamente',
            'un_favorite' => 'ha dejado de estar en tus favoritos correctamente'
    ],
    'push' => [
        'added_money_to_wallet' => 'Dinero agregado a tu billetera',
    ],
    'lang' => [
        'site_title' => 'Titulo del sitio',
        'site_logo' => 'Logo del sitio',
        'site_favicon' => 'Sitio Favicon',
        'site_copyright' => 'Derechos del sitio',
        'delivery_charge' => 'Costo de entrega',
        'resturant_response_time' => 'Tiempo de respuesta de la tienda',
        'currency' => 'Tipo de moneda',
        'manual_assign' => 'orden asignada',
        'search_distance' => 'Buscando distancia',
        'tax' => 'Tax',
        'transporter_response_time' => 'Tiempo de respuesta del domiciliario',
        'currency_code' => 'codigo del tipo de moneda',
        'GOOGLE_MAP_KEY' => 'GOOGLE MAP KEY',
        'payment_mode' => 'Tipo de pago'
    ]

    
];