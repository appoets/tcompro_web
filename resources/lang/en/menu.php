<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shop Language Lines
    |--------------------------------------------------------------------------
    */
    
    'admin' => [
        'dashboard' => 'Dashboard',
        'dispatcher' => 'Despachador',
        'dispute' => 'PQRS',
        'user_dispute' => 'USUARIOS CON PQRS',
        'db_dispute' => 'DB PQRS',
        'shop_dispute' => 'Tiendas con PQRS',
        'help_dispute' => 'Mensaje Comun Para Tiendas Con PQRS',
        'restaurant' => 'Tiendas',
        'list_restaurant' => 'Listas De Tiendas',
        'add_restaurant' => 'Adicionar Tiendas',
        'shop' => 'Tiendas',
        'list_shop' => 'Listas De Tiendas',
        'add_shop' => 'Adicionar Tiendas',
        'delivery_boy' => 'Tcompradores',
        'list_delivery_boy' => 'Lista De Tcompradores',
        'list_delivery_boy_shift' => 'Desplegar Detalles',
        'add_delivery_boy' => 'Adicionar Tcompradores',
        'dispute_manager' => 'Administrador De Pqrs',
        'list_dispute_manager' => 'Lista Del Administrador De PQRS',
        'add_dispute_manager' => 'Adicionar Administrador Para PQRS',
        'cuisines' => 'Tiendas',
        'list_cuisines' => 'Listado De Tiendas',
        'add_cuisines' => 'Adicionar Tienda',
        'promocode' => 'Codigos De Descuentos',
        'list_promocode' => 'Listas De Codigo De Descuento',
        'add_promocode' => 'Adicionar Codigos De Descuento',
        'resturant_banner' => 'Banner De la Tienda',
        'list_resturant_banner' => 'Lista De Banners De la tienda',
        'add_resturant_banner' => 'Adicionar Banner De La Tienda',
        'shop_banner' => 'Banner De La Tienda',
        'list_shop_banner' => 'Lista De Banner De La Tienda',
        'add_shop_banner' => 'Agregar Banner De La Tienda',
        'notice_board' => 'Avisos',
        'list_notice_board' => 'Lista De Avisos',
        'add_notice_board' => 'Adicionar Aviso',
        'customer' => 'Usuarios',
        'list_customer' => 'Lista De Usuarios',
        'add_customer' => 'Agregar Usuario',
        'delivery' => 'Entregas',
        'translation' => 'Traduccion',
        'pages'  => 'Pagina Web',
        'about' => 'Quienes Somos',
        'contact' => 'Contactanos',
        'terms' => 'Terminos Y Condiciones',
        'help' => 'Hayuda',
        'privacy'=>'Privacy',
        'refund'=>'Cancelaciones Y Regreso de Dinero',
        'otherterms'=>'Otros Terminos',
        'queries'=>'consultas generales',
        'faq' => 'FAQ',
        'setting' => 'Configuracion',
        'site_setting' => 'Configuracion Del Sitio',
        'lead' => 'Leads',
        'email_template' => 'Plantilla Para Correo Electronico',
        'list_restuarant' => 'Clientes Potenciales Para La tienda',
        'list_transporters' => 'Tcompradores',
        'list_newsletter' => 'Boletin De Noticias',
        'list_emailtemplate' => 'Lista De Plantillas De Correo Electronico',
        'add_emailtemplate' => 'Agregar Plantillas De Correo Electronico',
        'custom_push' => 'Configurar Notificaciones Push'

    ],
    'shop' => [
        'dashboard' => 'Dashboard',
        'dispatcher' => 'Despachador',
        'restaurant' => 'Tienda',
        'category' => 'Categorias',
        'category_list' => 'Lista De Categorias',
        'add_category' => 'Agregar Categorias',
        'product' => 'Productos',
        'product_list' => 'Lista De Productos',
        'add_product' => 'Agregar Productos',
        'deliveries' => 'Entregas',
        'addons' => 'Addons',
        'addons_list' => 'Lista de Addons',
        'add_addons' => 'Adicionar Addons'
    ],
    'user' => [
        'restaurant' => 'Tienda',
        'home' => 'Inicio',
        'about_us' => 'Sobre Nosotros',
        'faq' => 'FAQ',
        'order_last' => 'Sus Pedidos',
        'order_current' => 'Ordenes Pendientes',
        'my_profile' => 'Mi Perfil',
        'logout' => 'Cerrar Sesion',
        'login' => 'Iniciar Sesion',
        'register' => 'Registrarse',
        'my_account' => 'Mi Cuenta',
        'address_details' => 'Detalles De La Direccion',
        'menu_mobile' => 'Menu Del Telefono',
        'change_password' => 'Change Password',
        'cards' => 'Administracion De Targetas De Credito',
        'contacts' => 'Contactos',
        'wallet_amount'=> 'Balance De Monedero',
        'terms_condition' => 'Terminos y Condiciones',
        'wallet' => 'Adicionar Dinero A La Billetara'
    ],
    

    
      

];
