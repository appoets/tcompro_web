<?php

return [

'gallery_heading' => '¨Tu Comida Fresca Y Con La Mejor Calidad Sin Salir De Casa ',
'gallery_description' => 'Encuentra Los Mejores Productos Y A Los Mejores Precios!',
'resturant_heading' => 'Elija La Mejor Opcion Para Sus Compras',
'resturant_description' => 'Lista de Las Tiendas Mas Poulares Cerca De Usted. ',
'highlight_heading' => 'Cientos De Productos De Su Tienda Mas Cercana  - [CTA]',
'highlight_description' => 'Las Frutas Y Verduras Mas Frescas Y mejor Seleccionadas! ',
'highlight_btn' => 'Ver Todas Las Tiendas',
'work_with_us' => 'Trabaja Con Nosotros',
'work_with_us_desc' => 'Obten Una Lista De Todas Las Tiendas Para Que Puedas Realizar Tus Compras.',
'shop_heading' => 'Conoce Mas Sobre Tus Tiendas',
'shop_earn' => 'Comienza A Generar Mas Clientes',
'shop_desc' => 'Danos Detalles Sobre Tu Tienda - Informacion Relacionada con Tu Negocio Y La Informacion Legal Del Mismo. Y comience a Recibir Muchos Pedidos De Nuestros Clientes .',
'db_heading' => 'Looking for a Delivery Person Gig?',
'db_earn' => 'Inicie A Ganar Dinero',
'db_desc' => 'Excelente¡ Comienza a ganar dinero con nosotros ahora Mismo de la manera mas sencilla posible, Comienza Diligenciando una sencilla informacion.  
',

'Repartidor' => [
	'title' => 'Informacion Personal',
	'name' => 'Nombre',
	'email' => 'Correo Electronico',
	'phone' => 'Numero Telefonico',
	'address' => 'Direccion',
	'created' => 'Solicitud enviada con exito '

],
'nav' => [
	'home' => 'Inicio',
	'dashboard' => 'Esperando Ordenes',
	'resturants' => 'Tiendas',
	'aboutus' => 'Quienes Somos',
	'faq' => 'FAQ',
	'orders' => 'Sus Pedidos',
	'profile' => 'Perfil',
	'changepassword' => 'Cambiar Contraseña',
	'useraddress' => 'Direccion',
	'payment' => 'Administrar tarjeta',
	'resturant' => 'Tienda',
	'details' => 'Detalles',
	'about' => 'Quienes Somos',
        'contact' => 'Contactanos',
        'terms' => 'Terminos y condiciones',
        'privacy'=>'Privacy',
        'faq' => 'FAQ',
	'cart' => 'Ordenes',
	'product' => 'Productos',
	'wallet' => 'Billetara',
	'enquiry-delivery' => 'Consultar Nuevos Repartidores',
	'user' => 'Usuario',
	'password' => 'Contraseña',
	'reset' => 'Cambiar Contraseña'

],
'Pagos' => [
	'need_help' => 'Necesito Ayuda?',
	'contact_number' => '+57 3502721483',
	'delivery_time_title' => 'Tiempo De Entrega',
	'delivery_time_desc' => 'En este momento su Mercado Esta siendo preparado por uno de nuestros especialistas, Recuerda La mejor Calidad en la Puerta de tu casa  ',
	'secure_payment_title' => 'Tiempo De entrega',
	'secure_payment_desc' => 'Sus Pagos se encuentran protegidos Vive tranquilo sin Salir De casa Nosotros Tcompramos Lo que necesites. ',
	'title' => 'Metodo De Pago',
	'add_new' => 'Agregar Nuevo metodo De Pago',
	'add_card' => 'Agregar Nueva Tarjeta De Credito',
	'add_new' => 'Agregar Nueva Tarjeta De Credito',
	'pay_now' => 'Pagar Ahora',
	'no_card' => 'No cuenta con una tarjeta de Credito Registrada Por favor registrar una ahora',
	

],
'footer' => [
	'get_app_on' => 'Obtener Aplicacion',
	'secure_payment' => 'Pagos Seguros Con',
	'about' => 'Acerca De',
	'newsletter' => 'Recibe noticias y novedades sobre nosotros',
	'newsletter_desc' => 'Unete a nuestro Pagina y recibe noticias, descuentas y actualizaciones de los mejores productos.',
	'settings' => 'Configuracion'

],
'how_it_works' => [
	'how_it_works_title' => 'Como funciona Tcompra',
	'step_one' => '1' ,
	'step_one_title' => 'Danos Tu Ubicacion',
	'step_one_desc' => 'Encuenta Supermercados,Droguerias, Tieneas, Frutas,verduras y todo lo que necesites para tu casa..',
	'step_two' => '2' ,
	'step_two_title' => 'Escoje La tienda Que necesites',
	'step_two_desc' => 'Escoja los Productos mas frescos y todo lo que necesite para que le llegue a la puerta de su casa en el menor tiempo posible y con la mayor calidad y frescura. ',
	'step_three' => '3' ,
	'step_three_title' => 'Podras Pagar con tu tarjeta de credito o en efectivo',
	'step_three_desc' => 'Realiza tu Pago facilmente con tu tarjeta de credito o simplemente pagale al Tcomprador cuando llegue a tu casa',
	'step_four' => '4' ,
	'step_four_title' => 'Si llegara mi Pedido?',
	'step_four_desc' => 'Tu comida sera entregada en el menor tiempo posible, ademas de que podras ver tu comprador donde se encuentra en tiempo real.'


],
'delivery_time' => 'Es el Tiempo que Tardara el Tcomprador en Realizar La entrega!'


];