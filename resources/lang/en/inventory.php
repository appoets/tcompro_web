<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inventory Language Lines
    |--------------------------------------------------------------------------
    */
    'cuisine' => [
        'title' => 'Tiendas',
        'add_title' => 'Crear Tiendas',
        'edit_title' => 'Editar Tiendas',
        'add_cuisine' => 'Agregar Tiendas',
        'sl_no' => 'Sl.No',
        'name' => 'Nombre',
        'action' => 'Action',
        'no_record_found' => 'Tienda No Encontrada'
    ],
    'category' => [
        'title' => 'Categorias',
        'add_title' => 'Crear Categoria',
        'edit_title' => 'Editar categorias',
        'add_category' => 'Adicionar categoriar',
        'sl_no' => 'Sl.No',
        'name' => 'Nombre',
        'desc' => 'Descripcion',
        'status' => 'Status',
        'resturant_name' => 'Nombre De La Tienda',
        'image' => 'Imagen',
        'action' => 'Action',
        'no_record_found' => 'Categoria No Encontrada',
        'sub_title' => 'Sub Categoria',
        'add_subcategory' => 'Adicionar Sub Categoria',
        'sub_no_record_found' => 'Sub Categoria No Encontrada',
        'position' => 'Orden De la Categoria'

    ],
    'product' => [
        'title' => 'Productos',
        'add_title' => 'Crear Productos',
        'edit_title' => 'Editar Productos',
        'add_product' => 'Adicionar Productos',
        'sl_no' => 'Sl.No',
        'name' => 'Nombre',
        'desc' => 'Descricion',
        'status' => 'estado',
        'resturant_name' => 'Tienda',
        'image' => 'Imagen',
        'action' => 'Action',
        'cuisine' => 'Tienda',
        'category' => 'Categoria',
        'no_record_found' => 'Producto No encontrado',
        'pricing_title' => 'Precio',
        'price' => 'Precio',
        'discount' => 'Descuento',
        'discount_type' => 'Tipo De Despuento',
        'currency' => 'Moneda',
        'featured' => 'El Producto Es Destacado',
        'featured_position' => 'Posicion Destacada del Producto',
        'featured_image' => 'Imagen Destacada',
        'featured_image_note' => 'Nota:- Por favor agregue una imagen del tamaño 252x152 Para Productos Destacados',
        'addons' => 'Lista De Complementos',
        'sub_category' => 'Sub Categoria',
        'product_position' => 'Orden Del Producto',
        'fixed' => 'Fijar',
        'addon_fixed' => 'Complemento Fijo',
        'out_of_stock' => 'Inventario Agotado'
 
    ],
    'addons' => [

        'title' => 'Addons',
        'add_title' => 'Crear Addon',
        'edit_title' => 'Editar Addon',
        'add_addon' => 'Adicior Addon',
        'sl_no' => 'Sl.No',
        'name' => 'Nombre',
        'image' => 'Imagen',
        'action' => 'Action',
        'no_record_found' => 'No Add-ons Found!',
        'created_success' => 'addons creados Correctamente',
        'updated_success' => 'Addons Actualizados correctamente',
        'shop' => 'Nombre De La Tienda',
        'fixed' => 'Addons Requeridos',
        'remove' => 'Addons Borrados Satisfactoriamente.',
        'not_remove'=> 'Usted no puede borrar este addon Porque este esta siendo usado.'

    ],
    'cart' => [
        'added' => 'Adicionado Correctamente'
    ]
       

    
      

];
