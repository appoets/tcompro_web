<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | TLas siguientes líneas de idioma se utilizan durante la autenticación para varios mensajes que necesitamos mostrar al usuario. 
      Usted es libre de modificar estas líneas de idioma de acuerdo con los requisitos de su aplicación
    |
    */

    'failed' => 'Los datos ingresados no concuerdan con nuestros registros por favor valide la informacion ingresada.',
    'throttle' => 'demasiados intentos de ingreso. por favor intente de nuevo en 60 segundos.',

];
