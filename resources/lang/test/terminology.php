<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Terminology
    |--------------------------------------------------------------------------
    */

    'shop' => 'tienda',
    'shops' => 'tiendas',

    'transporter' => 'Tcomprador',
    'transporters' => 'Tcompradores',

    'order' => 'Orden',
    'orders' => 'Ordenes',

    'category' => 'Categoria',
    'categories' => 'Categorias',

];
