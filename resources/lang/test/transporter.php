<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shop Language Lines
    |--------------------------------------------------------------------------
    */
    
    'index' => [
        'title' => 'lista de Tcompradores',
        'add_transporter' => 'Agregar Tcompradores',
        'sl_no' => 'Sl.No',
        'name' => 'Nombre',
        'email' => 'correo electronico',
        'image' => 'foto',
        'address' => 'direccion',
        'contact_details' => 'detalles de contacto',
        'rating' => 'calificacion',
        'action' => 'Acciones',
        'no_record_found' => 'No tenemos Tcompradores disponibles en este momento'
    ],
    'create' => [
        'title' => 'Crear Tcompradores',
        'image' => 'foto de la tienda / Logo',
        'name' => 'nombre',
        'email' => 'correo electronico',
        'cuisine' => 'tienda',
        'phone' => 'numero telefonico',
        'password' => 'clave',
        'confirm_password' => 'Confirmacion de clave',
        'status' => 'estado',
        'everyday' => 'horario de apertura de la tienda',
        'hours_opening' => 'apertura',
        'hours_closing' => 'cierre',
        'pure_veg' => '100% natural',
        'Min_Amount' => 'compra minima',
        'offer_percent' => 'porcentaje de oferta',
        'estimated_delivery_time' => 'tiempo estimado de entrega',
        'description' => 'Descripcion',
        'location' => 'ubicacion',
        'address' => 'direccion',
        'cancel' => 'Cancelar',
        'save' => 'Salvar',
        'unsettle' => 'UNSETTLE',
        'settle' => 'SETTLE',
        'country_code' => "codigo del pais",
        'profile_image' => 'imagen de perfil'
    ],
    'edit' => [
        'title' => 'Editar Tcompradores',
    ],
    'created_success' => 'el Tcomprador :name ha sido creado correctamente',
    'updated_success' => 'el Tcomprador :name actualizado correctamente',
    'removed_success' => 'el Tcomprador :name borrado correctamente',
    'order_settled_by_admin' => 'Your Shift is settled By admin',
    'shift_settle' => 'Delivery People :name Shift Settled Successfully',
    'shift' => [
        'shift_end_error' => 'no puedes tomar un descanso de tu orden hasta que la hallas entregado'

    ]

    
      

];
