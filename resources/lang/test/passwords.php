<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debera tener mas de 6 caracteres y coincidir con la confirmacion de la contraseña.',
    'reset' => 'Su contraseñal ha sido cambiada!',
    'sent' => 'Hemos enviado a su correo electronico el Link para cambiar su contraseña!',
    'token' => 'El Token para resetear su contraseña es invalido.',
    'user' => "No encontramos el correo electronico asociado a su cuenta.",

];
