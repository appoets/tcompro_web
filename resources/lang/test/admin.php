<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    */
    'demomode'=>'"caracteristica CRUD" el panel de administracion ha sido desabilitado en el modo demostracion. Esta función se habilitará en su producto cuando sea comprada, si usted tiene alguna pregunta no dude en ponerse en contacto con nuestro soporte 24/7 en soporte@tcompro.co.',
    
    'name' => 'FOODIE',

    'zones' => [
        'index' => [
            'title' => 'Zonas',
        ],
        'create' => [
            'title' => 'Crear Zonas',
        ],
        'edit' => [
            'title' => 'Editar Zonas',
        ],

    ],
    'dashboard' => [
        'order_id' => 'Id de la orden',
        'customer_name' => 'Nombre del cliente',
        'restaurant' => 'Nombre de la tienda',
        'delivery_people' => 'Tcomprador',
        'status' => 'Status',
        'amount' => 'Monto a Pagar'
    ],
    'push' => [
        'Push_Notification' => 'Notificacion Push',
        'Sent_to' => 'Enviar A',
        'Push_Now' => 'Enviar ahora',
        'Schedule_Push' => 'Programar Notificacion',
        'Condition' => 'Condicion',
        'Notification_History' => 'Historial De Notificaciones',
        'Sent_on' => 'Enviado',
        'message' => 'Mensaje',

    ],
    'id' => 'Id',
    'message' => 'Mensaje'

];
