<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shop Language Lines
    |--------------------------------------------------------------------------
    */
    
    'index' => [
        'title' => 'Tienda',
        'add_shop' => 'Agregar Tienda nueva',
        'sl_no' => 'Sl.No',
        'name' => 'Nombre',
        'image' => 'Imagen',
        'address' => 'Diereccion',
        'contact_details' => 'Detalle de Contactos',
        'rating' => 'Calificacion',
        'action' => 'Accion',
        'no_record_found' => 'No tenemos Tiendas Activas en el momento'
    ],
    'create' => [
        'title' => 'Crear Tiendas',
        'image' => 'Imagen de la tienda / Logo',
        'banner' => 'Banner de la tienda',
        'name' => 'Nombre',
        'email' => 'Correo Electronico',
        'cuisine' => 'Tienda',
        'phone' => 'Detalles de contacto',
        'password' => 'Contraseña',
        'confirm_password' => 'Confirmar Contraseña',
        'status' => 'estatus',
        'everyday' => 'Horarios de apertura de la tienda',
        'hours_opening' => 'Apertura',
        'hours_closing' => 'Cierre',
        'pure_veg' => 'es 100% natural',
        'Min_Amount' => 'Compra minima',
        'offer_percent' => 'Porcentaje de descuento',
        'estimated_delivery_time' => 'Tiempo estimado de entrega',
        'description' => 'Descripcion',
        'location' => 'Ubicacion',
        'address' => 'Direccion',
        'cancel' => 'Cancelar',
        'save' => 'Salvar',
        'rating' => 'Calificaciones',
        'fixed' => 'Fixed'
    ],
    'edit' => [
        'title' => 'Editar Tienda',
    ],
    'created_success' => 'Shop :name Created Successfully',
    'updated_success' => 'Shop :name Updated Successfully',
    'removed_success' => 'Shop :name Removed Successfully'

    
      

];
